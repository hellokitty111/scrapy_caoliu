## install dependency
`pip install -r requirements.txt`
## save result into json
`scrapy crawl caoliu -o items.json -t json`
## save to sqlite db
`scrapy crawl caoliu`
## see results
```
sqlite3 hacknews.db
.table
select * from hn
```
## screenshot
![screenshot.png](screenshot.png)

## file tree
```
.
├── hn
│   ├── hacknews.db
│   ├── __init__.py
│   ├── __init__.pyc
│   ├── items.py
│   ├── items.pyc
│   ├── pipelines.py
│   ├── pipelines.pyc
│   ├── settings.py
│   ├── settings.pyc
│   ├── spiders
│   │   ├── hacknews.db
│   │   ├── __init__.py
│   │   ├── __init__.pyc
│   │   ├── ycombinator.py
│   │   └── ycombinator.pyc
│   ├── sqlmanage.py
│   └── sqlmanage.pyc
├── README.md
├── requirements.txt
└── scrapy.cfg

2 directories, 19 files
```
