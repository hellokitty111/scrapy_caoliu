# -*- coding: utf-8 -*-
import scrapy
from hn.items import HnItem


class YcombinatorSpider(scrapy.Spider):
    name = "caoliu"
    allowed_domains = ["bearhk.info"]
    start_urls = (
        'http://cc.bearhk.info/thread0806.php?fid=22&search=&page=1',
    )

    def parse(self, response):
        pages = int(response.xpath('//div[@class="pages"]/a[@id="last"]/@href')[0].re(r'(\d+)')[-1])
        base_url = 'http://cc.bearhk.info/thread0806.php?fid=22&search=&page='
        for page in range(1, pages + 1):
            yield scrapy.Request(base_url + str(page), callback=self.parse_page)

    @staticmethod
    def parse_page(response):
        sites = response.xpath('//tr[@class="tr3 t_one"]//td[2]/h3')
        for site in sites:
            try:
                hn_item = HnItem()
                hn_item['title'] = site.xpath('a/text()').extract()[0]
                hn_item['link'] = 'http://cc.bearhk.info/' + site.xpath('a/@href').extract()[0]
            except Exception:
                continue
            yield hn_item
